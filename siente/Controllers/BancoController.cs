﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using siente.Models;

namespace siente.Controllers
{
    public class BancoController : Controller
    {
        private sienteEntities db = new sienteEntities();

        //
        // GET: /Banco/

        public ActionResult Index()
        {
            return View(db.Bancoes.ToList());
        }

        //
        // GET: /Banco/Details/5

        public ActionResult Details(Guid id)
        {
            Banco banco = db.Bancoes.Find(id);
            if (banco == null)
            {
                return HttpNotFound();
            }
            return View(banco);
        }

        //
        // GET: /Banco/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Banco/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Banco banco)
        {
            if (ModelState.IsValid)
            {
                banco.Id = Guid.NewGuid();
                db.Bancoes.Add(banco);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(banco);
        }

        //
        // GET: /Banco/Edit/5

        public ActionResult Edit(Guid id)
        {
            Banco banco = db.Bancoes.Find(id);
            if (banco == null)
            {
                return HttpNotFound();
            }
            return View(banco);
        }

        //
        // POST: /Banco/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Banco banco)
        {
            if (ModelState.IsValid)
            {
                db.Entry(banco).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(banco);
        }

        //
        // GET: /Banco/Delete/5

        public ActionResult Delete(Guid id)
        {
            Banco banco = db.Bancoes.Find(id);
            if (banco == null)
            {
                return HttpNotFound();
            }
            return View(banco);
        }

        //
        // POST: /Banco/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Banco banco = db.Bancoes.Find(id);
            db.Bancoes.Remove(banco);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}