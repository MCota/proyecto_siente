﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using siente.Models;

namespace siente.Controllers
{
    public class CamionController : Controller
    {
        private sienteEntities db = new sienteEntities();

        //
        // GET: /Camion/

        public ActionResult Index()
        {
            var camions = db.Camions.Include(c => c.CamionMarca).Include(c => c.Persona);
            return View(camions.ToList());
        }

        //
        // GET: /Camion/Details/5

        public ActionResult Details(Guid id)
        {
            Camion camion = db.Camions.Find(id);
            if (camion == null)
            {
                return HttpNotFound();
            }
            return View(camion);
        }

        //
        // GET: /Camion/Create

        public ActionResult Create(Guid ?id)
        {

            ViewBag.IdMarca = new SelectList(db.CamionMarcas, "Id", "Nombre");
            if (id == null)
            {
                ViewBag.IdPersona = new SelectList(db.Personas.Where(p => p.IsConcesionario), "Id", "Nombre");
            }
            else
            {
                ViewBag.IdPersona = new SelectList(db.Personas.Where(p => p.IsConcesionario && p.Id == id), "Id", "Nombre",id);
            }
            return View();
        }

        //
        // POST: /Camion/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Camion camion)
        {
            if (ModelState.IsValid)
            {
                camion.Id = Guid.NewGuid();
                db.Camions.Add(camion);
                db.SaveChanges();
                return RedirectToAction("Dashboard", "Persona", new { id = camion.IdPersona});
            }

            ViewBag.IdMarca = new SelectList(db.CamionMarcas, "Id", "Nombre", camion.IdMarca);
            var personas = from p in db.Personas where p.IsConcesionario == true select p;
            ViewBag.IdPersona = new SelectList(personas, "Id", "Nombre", camion.IdPersona);
            return View(camion);
        }

        //
        // GET: /Camion/Edit/5

        public ActionResult Edit(Guid id)
        {
            Camion camion = db.Camions.Find(id);
            if (camion == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdMarca = new SelectList(db.CamionMarcas, "Id", "Nombre", camion.IdMarca);
            var personas = from p in db.Personas where p.IsConcesionario == true select p;
            ViewBag.IdPersona = new SelectList(personas, "Id", "Nombre", camion.IdPersona);
            return View(camion);
        }

        //
        // POST: /Camion/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Camion camion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(camion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdMarca = new SelectList(db.CamionMarcas, "Id", "Nombre", camion.IdMarca);
            var personas = from p in db.Personas where p.IsConcesionario == true select p;
            ViewBag.IdPersona = new SelectList(personas, "Id", "Nombre", camion.IdPersona);
            return View(camion);
        }

        //
        // GET: /Camion/Delete/5

        public ActionResult Delete(Guid id)
        {
            Camion camion = db.Camions.Find(id);
            if (camion == null)
            {
                return HttpNotFound();
            }
            return View(camion);
        }

        //
        // POST: /Camion/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Camion camion = db.Camions.Find(id);
            db.Camions.Remove(camion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}