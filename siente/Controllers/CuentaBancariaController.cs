﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using siente.Models;

namespace siente.Controllers
{
    public class CuentaBancariaController : Controller
    {
        private sienteEntities db = new sienteEntities();

        //
        // GET: /CuentaBancaria/

        public ActionResult Index()
        {
            var cuentabancarias = db.CuentaBancarias.Include(c => c.Banco).Include(c => c.Persona);
            return View(cuentabancarias.ToList());
        }

        //
        // GET: /CuentaBancaria/Details/5

        public ActionResult Details(Guid id)
        {
            CuentaBancaria cuentabancaria = db.CuentaBancarias.Find(id);
            if (cuentabancaria == null)
            {
                return HttpNotFound();
            }
            return View(cuentabancaria);
        }

        //
        // GET: /CuentaBancaria/Create

        public ActionResult Create(Guid ?id)
        {
            ViewBag.IdBanco = new SelectList(db.Bancoes, "Id", "Nombre");
            if (id == null)
            {
                ViewBag.IdPersona = new SelectList(db.Personas, "Id", "Nombre");
            }
            else
            {
                ViewBag.IdPersona = new SelectList(db.Personas.Where(p => p.Id == id), "Id", "Nombre");
            }
            return View();
        }

        //
        // POST: /CuentaBancaria/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CuentaBancaria cuentabancaria)
        {
            if (ModelState.IsValid)
            {
                cuentabancaria.Id = Guid.NewGuid();
                db.CuentaBancarias.Add(cuentabancaria);
                db.SaveChanges();

                if (db.Personas.Where(p => p.Id == cuentabancaria.IdPersona).First().IsConcesionario)
                {
                    return RedirectToAction("Dashboard", "persona", new { id = cuentabancaria.Persona.Id });
                }

                return RedirectToAction("Index");
            }

            ViewBag.IdBanco = new SelectList(db.Bancoes, "Id", "Nombre", cuentabancaria.IdBanco);
            ViewBag.IdPersona = new SelectList(db.Personas, "Id", "Nombre", cuentabancaria.IdPersona);


            return View(cuentabancaria);
        }

        //
        // GET: /CuentaBancaria/Edit/5

        public ActionResult Edit(Guid id)
        {
            CuentaBancaria cuentabancaria = db.CuentaBancarias.Find(id);
            if (cuentabancaria == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdBanco = new SelectList(db.Bancoes, "Id", "Nombre", cuentabancaria.IdBanco);
            ViewBag.IdPersona = new SelectList(db.Personas, "Id", "Nombre", cuentabancaria.IdPersona);
            return View(cuentabancaria);
        }

        //
        // POST: /CuentaBancaria/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CuentaBancaria cuentabancaria)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cuentabancaria).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdBanco = new SelectList(db.Bancoes, "Id", "Nombre", cuentabancaria.IdBanco);
            ViewBag.IdPersona = new SelectList(db.Personas, "Id", "Nombre", cuentabancaria.IdPersona);
            return View(cuentabancaria);
        }

        //
        // GET: /CuentaBancaria/Delete/5

        public ActionResult Delete(Guid id )
        {
            CuentaBancaria cuentabancaria = db.CuentaBancarias.Find(id);
            if (cuentabancaria == null)
            {
                return HttpNotFound();
            }
            return View(cuentabancaria);
        }

        //
        // POST: /CuentaBancaria/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            CuentaBancaria cuentabancaria = db.CuentaBancarias.Find(id);
            db.CuentaBancarias.Remove(cuentabancaria);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}