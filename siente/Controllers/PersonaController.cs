﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using siente.Models;

namespace siente.Controllers
{
    public class PersonaController : Controller
    {
        private sienteEntities db = new sienteEntities();

        //
        // GET: /Persona/

        public ActionResult Index()
        {
            return View(db.Personas.ToList());
        }

        //
        // GET: /Persona/Details/5

        public ActionResult Details(Guid id)
        {
            Persona persona = db.Personas.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        //
        // GET: /Persona/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Persona/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Persona persona)
        {
            if (ModelState.IsValid)
            {
                persona.Id = Guid.NewGuid();
                db.Personas.Add(persona);
                db.SaveChanges();
                return RedirectToAction("Create","CuentaBancaria", new { id = persona.Id} );
                //return RedirectToAction("Dashboard",new { id = persona.Id});
            }

            return View(persona);
        }

        //
        // GET: /Persona/Edit/5
         
        public ActionResult Edit(Guid id)
        {
            Persona persona = db.Personas.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        //
        // POST: /Persona/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Persona persona)
        {
            if (ModelState.IsValid)
            {
                db.Entry(persona).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(persona);
        }

        //
        // GET: /Persona/Delete/5

        public ActionResult Delete(Guid id)
        {
            Persona persona = db.Personas.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        //
        // POST: /Persona/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Persona persona = db.Personas.Find(id);
            db.Personas.Remove(persona);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Dashboard(Guid id, String error = "")
        {
            var persona = from p in db.Personas where p.Id == id  select p ;

            if (!persona.First().IsConcesionario)
            {
                return RedirectToAction("Create", "CuentaBancaria", new { id = id });
            }
            else
            {
                ViewBag.Camiones = db.Camions.Where(c => c.IdPersona == id).ToList();
                ViewBag.Concesiones = db.Concesions.Where(c => c.Persona1.Id == id).ToList();
                ViewBag.Permisos = db.Permisoes.Where(c => c.Concesion.Persona1.Id == id).ToList();
                ViewBag.Error = error;
                return View(persona.First());  
            }
            
        }
    }
}