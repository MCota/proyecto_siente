﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using siente.Models;

namespace siente.Controllers
{
    public class HistoriaCamionController : Controller
    {
        private sienteEntities db = new sienteEntities();

        //
        // GET: /HistoriaCamion/

        public ActionResult Index()
        {
            var historiacamions = db.HistoriaCamions.Include(h => h.Camion).Include(h => h.Concesion).Include(h => h.Concesion1);
            return View(historiacamions.ToList());
        }

        //
        // GET: /HistoriaCamion/Details/5

        public ActionResult Details(Guid id )
        {
            HistoriaCamion historiacamion = db.HistoriaCamions.Find(id);
            if (historiacamion == null)
            {
                return HttpNotFound();
            }
            return View(historiacamion);
        }

        //
        // GET: /HistoriaCamion/Create

        public ActionResult Create()
        {
            ViewBag.IdCamion = new SelectList(db.Camions, "Id", "Modelo");
            ViewBag.IdConcesionNva = new SelectList(db.Concesions, "Id", "Id");
            ViewBag.IdConcesionVieja = new SelectList(db.Concesions, "Id", "Id");
            return View();
        }

        //
        // POST: /HistoriaCamion/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HistoriaCamion historiacamion)
        {
            if (ModelState.IsValid)
            {
                historiacamion.Id = Guid.NewGuid();
                db.HistoriaCamions.Add(historiacamion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdCamion = new SelectList(db.Camions, "Id", "Modelo", historiacamion.IdCamion);
            ViewBag.IdConcesionNva = new SelectList(db.Concesions, "Id", "Id", historiacamion.IdConcesionNva);
            ViewBag.IdConcesionVieja = new SelectList(db.Concesions, "Id", "Id", historiacamion.IdConcesionVieja);
            return View(historiacamion);
        }

        //
        // GET: /HistoriaCamion/Edit/5

        public ActionResult Edit(Guid id )
        {
            HistoriaCamion historiacamion = db.HistoriaCamions.Find(id);
            if (historiacamion == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdCamion = new SelectList(db.Camions, "Id", "Modelo", historiacamion.IdCamion);
            ViewBag.IdConcesionNva = new SelectList(db.Concesions, "Id", "Id", historiacamion.IdConcesionNva);
            ViewBag.IdConcesionVieja = new SelectList(db.Concesions, "Id", "Id", historiacamion.IdConcesionVieja);
            return View(historiacamion);
        }

        //
        // POST: /HistoriaCamion/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HistoriaCamion historiacamion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(historiacamion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdCamion = new SelectList(db.Camions, "Id", "Modelo", historiacamion.IdCamion);
            ViewBag.IdConcesionNva = new SelectList(db.Concesions, "Id", "Id", historiacamion.IdConcesionNva);
            ViewBag.IdConcesionVieja = new SelectList(db.Concesions, "Id", "Id", historiacamion.IdConcesionVieja);
            return View(historiacamion);
        }

        //
        // GET: /HistoriaCamion/Delete/5

        public ActionResult Delete(Guid id)
        {
            HistoriaCamion historiacamion = db.HistoriaCamions.Find(id);
            if (historiacamion == null)
            {
                return HttpNotFound();
            }
            return View(historiacamion);
        }

        //
        // POST: /HistoriaCamion/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            HistoriaCamion historiacamion = db.HistoriaCamions.Find(id);
            db.HistoriaCamions.Remove(historiacamion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}