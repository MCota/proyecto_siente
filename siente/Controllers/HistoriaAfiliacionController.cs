﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using siente.Models;

namespace siente.Controllers
{
    public class HistoriaAfiliacionController : Controller
    {
        private sienteEntities db = new sienteEntities();

        //
        // GET: /HistoriaAfiliacion/

        public ActionResult Index()
        {
            var historiaafiliacions = db.HistoriaAfiliacions.Include(h => h.Concesion).Include(h => h.Empresa).Include(h => h.Empresa1);
            return View(historiaafiliacions.ToList());
        }

        //
        // GET: /HistoriaAfiliacion/Details/5

        public ActionResult Details(Guid id)
        {
            HistoriaAfiliacion historiaafiliacion = db.HistoriaAfiliacions.Find(id);
            if (historiaafiliacion == null)
            {
                return HttpNotFound();
            }
            return View(historiaafiliacion);
        }

        //
        // GET: /HistoriaAfiliacion/Create

        public ActionResult Create()
        {
            ViewBag.IdConcesion = new SelectList(db.Concesions, "Id", "Id");
            ViewBag.IdEmpresaVieja = new SelectList(db.Empresas, "Id", "Nombre");
            ViewBag.IdEmpresaNva = new SelectList(db.Empresas, "Id", "Nombre");
            return View();
        }

        //
        // POST: /HistoriaAfiliacion/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HistoriaAfiliacion historiaafiliacion)
        {
            if (ModelState.IsValid)
            {
                historiaafiliacion.Id = Guid.NewGuid();
                db.HistoriaAfiliacions.Add(historiaafiliacion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdConcesion = new SelectList(db.Concesions, "Id", "Id", historiaafiliacion.IdConcesion);
            ViewBag.IdEmpresaVieja = new SelectList(db.Empresas, "Id", "Nombre", historiaafiliacion.IdEmpresaVieja);
            ViewBag.IdEmpresaNva = new SelectList(db.Empresas, "Id", "Nombre", historiaafiliacion.IdEmpresaNva);
            return View(historiaafiliacion);
        }

        //
        // GET: /HistoriaAfiliacion/Edit/5

        public ActionResult Edit(Guid id)
        {
            HistoriaAfiliacion historiaafiliacion = db.HistoriaAfiliacions.Find(id);
            if (historiaafiliacion == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdConcesion = new SelectList(db.Concesions, "Id", "Id", historiaafiliacion.IdConcesion);
            ViewBag.IdEmpresaVieja = new SelectList(db.Empresas, "Id", "Nombre", historiaafiliacion.IdEmpresaVieja);
            ViewBag.IdEmpresaNva = new SelectList(db.Empresas, "Id", "Nombre", historiaafiliacion.IdEmpresaNva);
            return View(historiaafiliacion);
        }

        //
        // POST: /HistoriaAfiliacion/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HistoriaAfiliacion historiaafiliacion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(historiaafiliacion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdConcesion = new SelectList(db.Concesions, "Id", "Id", historiaafiliacion.IdConcesion);
            ViewBag.IdEmpresaVieja = new SelectList(db.Empresas, "Id", "Nombre", historiaafiliacion.IdEmpresaVieja);
            ViewBag.IdEmpresaNva = new SelectList(db.Empresas, "Id", "Nombre", historiaafiliacion.IdEmpresaNva);
            return View(historiaafiliacion);
        }

        //
        // GET: /HistoriaAfiliacion/Delete/5

        public ActionResult Delete(Guid id)
        {
            HistoriaAfiliacion historiaafiliacion = db.HistoriaAfiliacions.Find(id);
            if (historiaafiliacion == null)
            {
                return HttpNotFound();
            }
            return View(historiaafiliacion);
        }

        //
        // POST: /HistoriaAfiliacion/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            HistoriaAfiliacion historiaafiliacion = db.HistoriaAfiliacions.Find(id);
            db.HistoriaAfiliacions.Remove(historiaafiliacion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}