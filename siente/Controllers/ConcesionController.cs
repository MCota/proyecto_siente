﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using siente.Models;

namespace siente.Controllers
{
    public class ConcesionController : Controller
    {
        private sienteEntities db = new sienteEntities();

        //
        // GET: /Concesion/

        public ActionResult Index()
        {
            var concesions = db.Concesions.Include(c => c.Empresa).Include(c => c.Camion).Include(c => c.Persona).Include(c => c.Persona1);
            return View(concesions.ToList());
        }

        //
        // GET: /Concesion/Details/5

        public ActionResult Details(Guid id)
        {
            Concesion concesion = db.Concesions.Find(id);
            if (concesion == null)
            {
                return HttpNotFound();
            }
            return View(concesion);
        }

        //
        // GET: /Concesion/Create

        //public ActionResult Create()
        //{
        //    ViewBag.IdEmpresa = new SelectList(db.Empresas,"Id","Nombre");
        //    ViewBag.IdCamion = new SelectList(db.Camions, "Id", "IdTexto");
        //    ViewBag.IdBeneficiario = new SelectList(db.Personas, "Id", "Nombre");
        //    ViewBag.IdConcesionario = new SelectList(db.Personas.Where(x => x.IsConcesionario), "Id", "Nombre");
        //    return View();
        //}


        //
        // GET: /Concesion/Create/id/5

        public ActionResult Create(Guid ?id)
        {
            
            ViewBag.IdEmpresa = new SelectList(db.Empresas, "Id", "Nombre");
            ViewBag.IdBeneficiario = new SelectList(db.Personas, "Id", "Nombre");
            if(id != null){
                ViewBag.IdConcesionario = new SelectList(db.Personas.Where(p => p.Id == id), "Id", "Nombre", id);
                ViewBag.IdCamion = new SelectList(db.Camions.Where(c => c.IdPersona == id), "Id", "IdNum");
            }
            else{
                ViewBag.IdConcesionario = new SelectList(db.Personas.Where(x => x.IsConcesionario), "Id", "Nombre");
                ViewBag.IdCamion = new SelectList(db.Camions, "Id", "IdTexto");
            }
            return View();
        }
        //
        // POST: /Concesion/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Concesion concesion)
        {
            if (ModelState.IsValid)
            {
                concesion.Id = Guid.NewGuid();
                db.Concesions.Add(concesion);
                db.SaveChanges();
                return RedirectToAction("Dashboard", "Persona", new { id = concesion.IdConcesionario });
            }
            ViewBag.IdEmpresa = new SelectList(db.Empresas, "Id", "Nombre",concesion.IdEmpresa);
            ViewBag.IdCamion = new SelectList(db.Camions, "Id", "IdNum", concesion.IdCamion);
            ViewBag.IdBeneficiario = new SelectList(db.Personas, "Id", "Nombre", concesion.IdBeneficiario);
            ViewBag.IdConcesionario = new SelectList(db.Personas, "Id", "Nombre", concesion.IdConcesionario);
            return View(concesion);
        }

        //
        // GET: /Concesion/Edit/5

        public ActionResult Edit(Guid id )
        {
            Concesion concesion = db.Concesions.Find(id);
            if (concesion == null)
            {
                return HttpNotFound();
            }

            ViewBag.IdEmpresa = new SelectList(db.Empresas, "Id", "Nombre", concesion.IdEmpresa);
            ViewBag.IdCamion = new SelectList(db.Camions, "Id", "IdNum", concesion.IdCamion);
            ViewBag.IdBeneficiario = new SelectList(db.Personas, "Id", "Nombre", concesion.IdBeneficiario);
            ViewBag.IdConcesionario = new SelectList(db.Personas, "Id", "Nombre", concesion.IdConcesionario);
            return View(concesion);
        }

        //
        // POST: /Concesion/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Concesion concesion)
        {
            if (ModelState.IsValid)
            {
                var empresa = db.Concesions.Where(p => p.Id == concesion.Id).Select(p => new { empresa = p.IdEmpresa }).First();
                Guid idempresa = (Guid)empresa.empresa;
                var camion = db.Concesions.Where(p => p.Id == concesion.Id).Select(p => new { camion = p.IdCamion }).First();
                Guid idcamion = (Guid)camion.camion;
                db.Entry(concesion).State = EntityState.Modified;
                db.SaveChanges();
                if (idempresa != concesion.IdEmpresa)
                {
                    HistoriaAfiliacion historial = new HistoriaAfiliacion();
                    historial.Empresa = db.Empresas.Where(p => p.Id == concesion.IdEmpresa).First();
                    historial.Empresa1 = db.Empresas.Where(p => p.Id == idempresa).First();
                    historial.IdConcesion = concesion.Id;
                    historial.FechaRegistro = DateTime.Now;
                    Guid id = Guid.NewGuid();
                    historial.Id = id;
                    db.HistoriaAfiliacions.Add(historial);
                    db.SaveChanges();
                }
                if (idcamion != concesion.IdCamion)
                {
                    var concesions = db.Concesions.Where(p => p.IdCamion == concesion.IdCamion && p.Id != concesion.Id);
                    if (concesions.Count() > 0)
                    {
                        HistoriaCamion historial1 = new HistoriaCamion();
                        historial1.Camion = db.Camions.Where(p => p.Id == concesion.IdCamion).First();
                        historial1.Concesion = concesion;
                        historial1.Concesion1 = concesions.First();
                        historial1.FechaRegistro = DateTime.Now;
                        Guid id = Guid.NewGuid();
                        historial1.Id = id;
                        db.HistoriaCamions.Add(historial1);
                        db.SaveChanges();
                    }
                }


                return RedirectToAction("Index");
            }
            ViewBag.IdEmpresa = new SelectList(db.Empresas, "Id", "Nombre");
            ViewBag.IdCamion = new SelectList(db.Camions, "Id", "IdNum", concesion.IdCamion);
            ViewBag.IdBeneficiario = new SelectList(db.Personas, "Id", "Nombre", concesion.IdBeneficiario);
            ViewBag.IdConcesionario = new SelectList(db.Personas, "Id", "Nombre", concesion.IdConcesionario);
            return View(concesion);
        }

        //
        // GET: /Concesion/Delete/5

        public ActionResult Delete(Guid id)
        {
            Concesion concesion = db.Concesions.Find(id);
            if (concesion == null)
            {
                return HttpNotFound();
            }
            return View(concesion);
        }

        //
        // POST: /Concesion/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Concesion concesion = db.Concesions.Find(id);
            db.Concesions.Remove(concesion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        
        //
        // POST: /Concesion/getDatos/id/5
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult getDatos(string id = "")
        {
            Guid Id = Guid.Parse(id);
            var concesion = db.Concesions.Where(c => c.Id == Id).Select(c => new { id = c.Id, camion = c.Camion.IdNum , ruta = c.Ruta, beneficiario = c.Persona.Nombre, concesionario = c.Persona1.Nombre}).ToList();
            return Json(concesion,JsonRequestBehavior.AllowGet);
        }

        public ActionResult Usuarios()
        {
            var lista = db.Personas.Where(c => c.IsConcesionario).ToList();
            ViewBag.concesionarios = lista;
            return View();
        }
    }
}