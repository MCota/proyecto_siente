﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using siente.Models;

namespace siente.Controllers
{
    public class PermisoController : Controller
    {
        private sienteEntities db = new sienteEntities();

        //
        // GET: /Permiso/

        public ActionResult Index()
        {
            var permisoes = db.Permisoes.Include(p => p.Camion).Include(p => p.Concesion).Include(p => p.Seguro);
            return View(permisoes.ToList());
        }

        //
        // GET: /Permiso/Details/5

        public ActionResult Details(Guid id)
        {
            Permiso permiso = db.Permisoes.Find(id);
            if (permiso == null)
            {
                return HttpNotFound();
            }
            return View(permiso);
        }

        //
        // GET: /Permiso/Create

        public ActionResult Create(Guid ?id)
        {

            if (id == null)
            {
                ViewBag.IdCamion = new SelectList(db.Camions, "Id", "Modelo");
                var con = from c in db.Concesions select new { nombre = c.Persona1.Nombre, Id = c.Id };
                ViewBag.IdConcesion = new SelectList(con, "Id", "nombre");

                ViewBag.IdSeguro = new SelectList(db.Seguroes, "Id", "NumPoliza");
            }
            else
            {

                //ViewBag.IdCamion = new SelectList(db.Camions.Where(c => c.IdPersona == id && c.Seguroes.Where(s => s.FechaTermino < DateTime.Now).Count() > 0), "Id", "Modelo");
                ViewBag.IdConcesion = new SelectList(db.Concesions.Where(c => c.Camion.Seguroes.Where(s => s.FechaTermino > DateTime.Now ).Count() > 0 && c.Persona1.Id == id), "Id", "Id");// new SelectList(db.Concesions.Where(c => c.IdConcesionario == id && c.Camion.Seguroes.Where(s => s.FechaTermino < DateTime.Now).Count() > 0), "Id", "nombre");
                //ViewBag.IdSeguro = new SelectList(db.Seguroes, "Id", "NumPoliza");  //new SelectList(db.Seguroes.ToList().Where(s => s.Camion.Seguroes.Where(t => t.FechaTermino < DateTime.Now).Count() > 0), "Id", "NumPoliza");
            }
            return View();
        }

        //
        // POST: /Permiso/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Permiso permiso)
        {
            if (ModelState.IsValid)
            {
                permiso.Id = Guid.NewGuid();
                Persona persona = db.Concesions.Where(c => c.Id == permiso.IdConcesion).First().Persona1;
                int cantidad = (persona.IsFisica)? 3 : 5;
                var camion = db.Concesions.Where(c => c.Id == permiso.IdConcesion).First().Camion;
                permiso.IdCamion = camion.Id;

                if (camion.Seguroes.Last().FechaTermino >= DateTime.Now)
                {
                    if (db.Permisoes.Where(p => p.Camion.Id == camion.Id && p.FechaTermino >= DateTime.Now).Count() == 0 )
                    {
                        if (db.Permisoes.Where(p => p.FechaTermino >= DateTime.Now && p.Concesion.IdConcesionario == persona.Id).Count() <= cantidad)
                        {
                            permiso.IdSeguro = db.Camions.Where(c => c.Id == permiso.IdCamion).First().Seguroes.Where(s => s.FechaTermino > DateTime.Now).Last().Id;
                            db.Permisoes.Add(permiso);
                            db.SaveChanges();
                            return RedirectToAction("Dashboard", "Persona", new { id = db.Concesions.Where(c => c.Id == permiso.IdConcesion).First().Persona1.Id });
                        }
                        else
                        {
                            return RedirectToAction("Dashboard", "Persona", new { id = persona.Id, error = "El concesionario ya cuenta con el limite de permisos posibles" });
                        }
                    }
                    else
                    {
                        return RedirectToAction("Dashboard", "Persona", new { id = persona.Id , error = "El camion al que intenta dar permiso ya cuenta con un permiso activo" });
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Persona", new { id = persona.Id });
                }
            }

            ViewBag.IdCamion = new SelectList(db.Camions, "Id", "Modelo", permiso.IdCamion);
            var con = from c in db.Concesions select new { nombre = c.Persona1.Nombre, Id = c.Id };
            ViewBag.IdConcesion = new SelectList(con, "Id", "nombre");
            ViewBag.IdSeguro = new SelectList(db.Seguroes, "Id", "NumPoliza", permiso.IdSeguro);
            return View(permiso);
        }

        //
        // GET: /Permiso/Edit/5

        public ActionResult Edit(Guid id)
        {
            Permiso permiso = db.Permisoes.Find(id);
            if (permiso == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdCamion = new SelectList(db.Camions, "Id", "Modelo", permiso.IdCamion);
            var con = from c in db.Concesions select new { nombre = c.Persona1.Nombre, Id = c.Id };
            ViewBag.IdConcesion = new SelectList(db.Concesions, "Id", "Persona1.Nombre");
            ViewBag.IdSeguro = new SelectList(db.Seguroes, "Id", "NumPoliza", permiso.IdSeguro);
            return View(permiso);
        }

        //
        // POST: /Permiso/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Permiso permiso)
        {
            if (ModelState.IsValid)
            {
                db.Entry(permiso).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdCamion = new SelectList(db.Camions, "Id", "Modelo", permiso.IdCamion);
            var con = from c in db.Concesions select new { nombre = c.Persona1.Nombre, Id = c.Id };
            ViewBag.IdConcesion = new SelectList(con, "Id", "nombre");
            ViewBag.IdSeguro = new SelectList(db.Seguroes, "Id", "NumPoliza", permiso.IdSeguro);
            return View(permiso);
        }

        //
        // GET: /Permiso/Delete/5

        public ActionResult Delete(Guid id)
        {
            Permiso permiso = db.Permisoes.Find(id);
            if (permiso == null)
            {
                return HttpNotFound();
            }
            return View(permiso);
        }

        //
        // POST: /Permiso/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Permiso permiso = db.Permisoes.Find(id);
            db.Permisoes.Remove(permiso);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}