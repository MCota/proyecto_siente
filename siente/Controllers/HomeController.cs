﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using siente.Models;
using System.Reflection;
namespace siente.Controllers
{
    public class HomeController : Controller
    {
        private sienteEntities db = new sienteEntities();
        public ActionResult Index()
        {
            //fecha final de final de la concesioon esta a por lo menos una semana de terminar 
            var actual = DateTime.Now;
            var anterior = actual.AddDays(-7);
            var lista = from p in db.Permisoes where p.FechaTermino >= anterior select p;
            anterior.ToString();
            ViewBag.Avisos = lista;
            return View();
        }
         
        public ActionResult About()
        {
            ViewBag.Message = "Página de descripción de la aplicación.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Página de contacto.";

            return View();
        }
    }
}
