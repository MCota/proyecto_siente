﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using siente.Models;

namespace siente.Controllers
{
    public class SeguroController : Controller
    {
        private sienteEntities db = new sienteEntities();

        //
        // GET: /Seguro/

        public ActionResult Index()
        {
            var seguroes = db.Seguroes.Include(s => s.Camion);
            return View(seguroes.ToList());
        }

        //
        // GET: /Seguro/Details/5

        public ActionResult Details(Guid id)
        {
            Seguro seguro = db.Seguroes.Find(id);
            if (seguro == null)
            {
                return HttpNotFound();
            }
            return View(seguro);
        }

        //
        // GET: /Seguro/Create/5

        public ActionResult Create(Guid ?id)
        {
            if (id == null)
            {
                ViewBag.IdCamion = new SelectList(db.Camions, "Id", "Modelo");
            }
            else
            {
                var obj = db.Camions.Where(s => s.Id == id).First();
                List<SelectListItem> lista = new List<SelectListItem>();
                lista.Add(new SelectListItem { Text = obj.IdNum.ToString(), Value = obj.Id.ToString(), Selected = true });

                ViewBag.IdCamion = new SelectList(db.Camions.Where(s => s.Id == id), "Id", "IdNum", id);
            }
            return View();
        }

        //
        // POST: /Seguro/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Seguro seguro)
        {
            if (ModelState.IsValid)
            {
                seguro.Id = Guid.NewGuid();
                db.Seguroes.Add(seguro);
                db.SaveChanges();
                return RedirectToAction("Dashboard", "Persona", new { id = db.Camions.Where(c => c.Id == seguro.IdCamion).First().IdPersona, error = ""});
            }

            ViewBag.IdCamion = new SelectList(db.Camions, "Id", "Modelo", seguro.IdCamion);
            return View(seguro);
        }

        //
        // GET: /Seguro/Edit/5

        public ActionResult Edit(Guid id)
        {
            Seguro seguro = db.Seguroes.Find(id);
            if (seguro == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdCamion = new SelectList(db.Camions, "Id", "Modelo", seguro.IdCamion);
            return View(seguro);
        }

        //
        // POST: /Seguro/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Seguro seguro)
        {
            if (ModelState.IsValid)
            {
                db.Entry(seguro).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdCamion = new SelectList(db.Camions, "Id", "Modelo", seguro.IdCamion);
            return View(seguro);
        }

        //
        // GET: /Seguro/Delete/5

        public ActionResult Delete(Guid id)
        {
            Seguro seguro = db.Seguroes.Find(id);
            if (seguro == null)
            {
                return HttpNotFound();
            }
            return View(seguro);
        }

        //
        // POST: /Seguro/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Seguro seguro = db.Seguroes.Find(id);
            db.Seguroes.Remove(seguro);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}