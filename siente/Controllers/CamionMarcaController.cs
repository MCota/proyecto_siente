﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using siente.Models;

namespace siente.Controllers
{
    public class CamionMarcaController : Controller
    {
        private sienteEntities db = new sienteEntities();

        //
        // GET: /CamionMarca/

        public ActionResult Index()
        {
            return View(db.CamionMarcas.ToList());
        }

        //
        // GET: /CamionMarca/Details/5

        public ActionResult Details(Guid id)
        {
            CamionMarca camionmarca = db.CamionMarcas.Find(id);
            if (camionmarca == null)
            {
                return HttpNotFound();
            }
            return View(camionmarca);
        }

        //
        // GET: /CamionMarca/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CamionMarca/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CamionMarca camionmarca)
        {
            if (ModelState.IsValid)
            {
                camionmarca.Id = Guid.NewGuid();
                db.CamionMarcas.Add(camionmarca);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(camionmarca);
        }

        //
        // GET: /CamionMarca/Edit/5

        public ActionResult Edit(Guid id)
        {
            CamionMarca camionmarca = db.CamionMarcas.Find(id);
            if (camionmarca == null)
            {
                return HttpNotFound();
            }
            return View(camionmarca);
        }

        //
        // POST: /CamionMarca/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CamionMarca camionmarca)
        {
            if (ModelState.IsValid)
            {
                db.Entry(camionmarca).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(camionmarca);
        }

        //
        // GET: /CamionMarca/Delete/5

        public ActionResult Delete(Guid id)
        {
            CamionMarca camionmarca = db.CamionMarcas.Find(id);
            if (camionmarca == null)
            {
                return HttpNotFound();
            }
            return View(camionmarca);
        }

        //
        // POST: /CamionMarca/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            CamionMarca camionmarca = db.CamionMarcas.Find(id);
            db.CamionMarcas.Remove(camionmarca);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}