﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace siente.Models
{
    [MetadataType(typeof(HistoriaCamionMetaData))]

    public partial class HistoriaCamion
    {
        public class HistoriaCamionMetaData
        {
            [Display(Name = "Fecha de Registro")]
            public DateTime FechaRegistro { get; set; }

            [Display(Name = "Concesión Anterior")]
            public Guid Concesion { get; set; }
            
            [Display(Name = "Concesión Nueva")]
            public Guid Concesion1 { get; set; }

        }
    }
}