﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace siente.Models
{
    [MetadataType(typeof(BancoMetaData))]

    public partial class Banco
    {
        public class BancoMetaData
        {
            [Required(ErrorMessage = "El nombre del banco es requerido")]
            public Guid Nombre { get; set; }
        }
    }
}