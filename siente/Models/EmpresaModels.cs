﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace siente.Models
{
    [MetadataType(typeof(EmpresaMetaData))]

    public partial class Empresa
    {
        public class EmpresaMetaData
        {
            [Required(ErrorMessage = "El nombre de la empresa es requerido")]
            public string Nombre { get; set; }
        }
    }
}