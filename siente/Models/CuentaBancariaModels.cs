﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace siente.Models
{
    [MetadataType(typeof(CuentaBancariaMetaData))]

    public partial class CuentaBancaria
    {
        public class CuentaBancariaMetaData
        {
            [Required(ErrorMessage = "El nombre de la cuenta es requerido")]
            public string Nombre { get; set; }

            [Required(ErrorMessage = "El número CLABE es requerido")]
            public string CLABE { get; set; }

            [Display(Name = "Persona")]
            [Required(ErrorMessage = "El nombre de la persona es requerido")]
            public Guid IdPersona { get; set; }
            public Guid Persona { get; set; }

            [Display(Name = "Banco")]
            [Required(ErrorMessage = "El nombre del banco es requerido")]
            public Guid IdBanco { get; set; }
            public Guid Banco { get; set; }


        }
    }
}