﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
namespace siente.Models
{
    [MetadataType(typeof(HistoriaAfiliacionMetaData))]
    
    public partial class HistoriaAfiliacion
    {
        public class HistoriaAfiliacionMetaData
        {
            [Display(Name = "Fecha de Registro")]
            public DateTime FechaRegistro { get; set; }
        }
    }
}