﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace siente.Models
{
    [MetadataType(typeof(SeguroMetaData))]

    public partial class Seguro
    {
        public class SeguroMetaData
        {
            [Display(Name = "Camión")]
            public Guid Camion { get; set; }

            [Required(ErrorMessage = "El número del camión es requerido")]
            [Display(Name = "Camión")]
            public Guid IdCamion { get; set; }

            [Required(ErrorMessage = "El número de poliza es requerido")]
            [Display(Name = "Número Poliza")]
            public string NumPoliza { get; set; }

            [Required(ErrorMessage = "El número es requerido")]
            [Display(Name = "Número")]
            public string NumEndoso { get; set; }

            [Required(ErrorMessage = "La fecha de inicio es requerida")]
            [Display(Name = "Fecha Inicio")]
            [DataType(DataType.DateTime,ErrorMessage="Introduce una fecha válida")]
            public DateTime FechaInicio { get; set; }

            [Required(ErrorMessage = "La fecha de término es requerida")]
            [Display(Name = "Fecha Término")]
            [DataType(DataType.DateTime, ErrorMessage = "Introduce una fecha válida")]
            public DateTime FechaTermino { get; set; }
        }
    }
}