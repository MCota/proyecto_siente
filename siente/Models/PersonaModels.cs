﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace siente.Models
{

    [MetadataType(typeof(PersonaMetaData))]

    public partial class Persona
    {
        public class PersonaMetaData
        {
            [Required(ErrorMessage = "El nombre de la persona es requerido")]
            public Guid Nombre { get; set; }

            [Display(Name = "Persona Física")]
            public bool IsFisica { get; set; }

            [Display(Name = "Concesionario")]
            public bool IsConcesionario { get; set; }
        }
    }
}