﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace siente.Models
{

    [MetadataType(typeof(ConcesionMetaData))]

    public partial class Concesion
    {
        public class ConcesionMetaData
        {
            [Required(ErrorMessage = "La ruta del camión es requerida")]
            [Range(1, 19, ErrorMessage = "La rutas son del 1 al 19")]
            public int Ruta { get; set; }

            [Display(Name = "Empresa")]
            public Guid Empresa { get; set; }

            [Required(ErrorMessage = "El nombre de la empresa es requerido")]
            [Display(Name = "Empresa")]
            public Guid IdEmpresa { get; set; }

            [Display(Name = "Camión")]
            public Guid Camion { get; set; }

            [Required(ErrorMessage = "El número del camión es requerido")]
            [Display(Name = "Camión")]
            public Guid IdCamion { get; set; }

            [Display(Name = "Concesionario")]
            public Guid Persona1 { get; set; }

            [Required(ErrorMessage = "El nombre del concesionario es requerido")]
            [Display(Name = "Concesionario")]
            public Guid IdConcesionario { get; set; }

            [Display(Name = "Beneficiario")]
            public Guid Persona { get; set; }

            [Required(ErrorMessage = "El nombre del beneficiario es requerido")]
            [Display(Name = "Beneficiario")]
            public Guid IdBeneficiario { get; set; }
        }
    }
}