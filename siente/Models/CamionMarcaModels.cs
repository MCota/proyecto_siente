﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace siente.Models
{
    [MetadataType(typeof(CamionMarcaMetaData))]

    public partial class CamionMarca
    {
        public class CamionMarcaMetaData
        {
            [Required(ErrorMessage = "El nombre de la marca es requerido")]
            public Guid Nombre { get; set; }
        }
    }
}