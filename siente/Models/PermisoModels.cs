﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace siente.Models
{

    [MetadataType(typeof(PermisoMetaData))]

    public partial class Permiso
    {
        public class PermisoMetaData
        {
            [Required(ErrorMessage = "La fecha de inicio es requerida")]
            [Display(Name = "Fecha Inicio")]
            [DataType(DataType.DateTime, ErrorMessage = "Introduce una fecha válida")]
            public DateTime FechaInicio { get; set; }

            [Required(ErrorMessage = "La fecha de término es requerida")]
            [Display(Name = "Fecha Término")]
            [DataType(DataType.DateTime, ErrorMessage = "Introduce una fecha válida")]
            public DateTime FechaTermino { get; set; }

            [Required(ErrorMessage = "El número de la poliza es requerido")]
            [Display(Name = "Número de Poliza")]
            public Guid IdSeguro { get; set; }

            [Display(Name = "Camión")]
            public Guid Camion { get; set; }

            [Required(ErrorMessage = "El número del camión es requerido")]
            [Display(Name = "Camión")]
            public Guid IdCamion { get; set; }

            [Display(Name = "Concesión")]
            public Guid Concesion { get; set; }

            [Required(ErrorMessage = "El nombre del concesionario es requerido")]
            [Display(Name = "Concesión")]
            public Guid IdConcesion { get; set; }
        }
    }
}