//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace siente.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class HistoriaCamion
    {
        public System.Guid Id { get; set; }
        public System.DateTime FechaRegistro { get; set; }
        public System.Guid IdConcesionNva { get; set; }
        public System.Guid IdConcesionVieja { get; set; }
        public System.Guid IdCamion { get; set; }
    
        public virtual Camion Camion { get; set; }
        public virtual Concesion Concesion { get; set; }
        public virtual Concesion Concesion1 { get; set; }
    }
}
