﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace siente.Models
{

    [MetadataType(typeof(CamionMetaData))]

    public partial class Camion
    {
        public class CamionMetaData
        {
            [Display(Name = "Marca Camión")]
            public Guid CamionMarca { get; set; }

            [Required(ErrorMessage = "La Marca del camión es requerida")]
            [Display(Name="Marca Camión")]
            public Guid IdMarca { get; set; }

            [Required(ErrorMessage = "El año del camión es requirido")]
            [Display(Name = "Año")]
            public int Ano { get; set; }

            [Required(ErrorMessage = "El número del camión es requerido")]
            [Display(Name = "Número")]
            public int IdNum { get; set; }

            [Display(Name = "Concesionario")]
            public Guid Persona { get; set; }

            [Required(ErrorMessage = "El nombre del concesionario es requerido")]
            [Display(Name = "Concesionario")]
            public Guid IdPersona { get; set; }
        }
    }
}